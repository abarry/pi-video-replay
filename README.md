# Video Replay on the Raspberry Pi

## Features
- Slow motion video replay
- Easy to use: automatic startup, video recording, and looping.
- Triggering via USB slide remote or connection to Favero scoring machine

## Triggering from Favero scoring machine via GPIO board:

![PCB Photo](circuit-board.jpg)
- [Board schematic](https://gitlab.com/abarry/pi-video-replay/tree/master/favero_serial_pcb)


## Installation

1. Swap serial ports by putting this in `/boot/config.txt`

    ```
    dtoverlay=pi3-miniuart-bt
    ```


2. Disable serial console

    ```
    sudo systemctl stop serial-getty@ttyAMA0.service
    sudo systemctl disable serial-getty@ttyAMA0.service
    ```

    And edit `/boot/cmdline.txt`

    remove the text: `console=serial0,115200` and save.


3. Enable camera:
    ````
    sudo raspi-config
    > Interfacing Options > P1 Camera > Enable
    ````

4. Setup a RAM disk on boot
    
    ````
    sudo mkdir /mnt/rd
    ````

    Add to ````/etc/rc.local```` above ````exit 0````
    ````
    # Create ramdisk
    /bin/mount -t tmpfs -o size=256M,mode=700 tmpfs /mnt/rd/
    /bin/chown pi:root /mnt/rd
    /bin/chmod 0750 /mnt/rd
    ````

5. Reboot.
    

6. Install MP4Box
    ````
    sudo apt-get install gpac
    ````


7. Install pngview

    Download or clone https://github.com/AndrewFromMelbourne/raspidmx
    ````
    git clone https://github.com/AndrewFromMelbourne/raspidmx.git
    cd raspidmx
    make

    sudo cp lib/lib* /usr/lib
    sudo cp pngview/pngview /usr/local/bin/
    ````

8. Download this software:
    ````
    git clone https://gitlab.com/abarry/pi-video-replay.git
    ````

9. Test the system:
    ````
    cd pi-video-replay
    python video.py -f
    ````
    
    You can manually trigger a replay with 'b' and quit with 'q'.

10. Install autostart script:
    ````
    mkdir -p $HOME/.config/autostart
    ln -s `pwd`/video-start.desktop $HOME/.config/autostart
    ````


---

Based on the [Pinewood Derby Instant Replay System](https://www.youtube.com/watch?v=-QyMxKfBaAE) by Andre Miron
