import serial
import time
import threading


class ReadFaveroSerial(threading.Thread):
    def __init__(self, callback_function):
        threading.Thread.__init__(self)
        self.serial_port = serial.Serial('/dev/ttyAMA0', 2400, timeout=1)
        self.done = False
        self.callback_function = callback_function
        
    def clean_up(self):
        self.done = True
        
    def run(self):
            
        reading = False
        count = 0
        b = []
        
        while not self.done:
            q = self.serial_port.read(1)
            
            if len(q) <= 0:
                # Failed to read
                print "Failed to read port"
                continue
            
            if not reading and ord(q) == 255:
                count = 0
                reading = True
                b = []
                #print ''

                
            #print ord(q),
            b.append(ord(q))
                
            count = count + 1
            
            if count >= 10:
                reading = False
                data = self.decodeBytes(b)
                
                if data is not None:
                    self.callback_function(data)
                    
        self.serial_port.close()
            

            
    def decodeBytes(self, b):
        data = {}
        # b[0] = 255
        if b[0] != 255:
            print "WARNING: byte string does not start with 255"
            return None
            
        if len(b) != 10:
            print "WARNING: len(b) != 10"
            return None
            
        # Byte 2 is the right score
        data['right_score'] = b[1]
        
        data['left_score'] = b[2]
        
        data['time_sec'] = b[3]
        
        data['time_min'] = b[4]
        
        data['left_white'] =    bool(0b00000001 & b[5])
        data['right_white'] =     bool(0b00000010 & b[5])
        data['red'] =             bool(0b00000100 & b[5])
        data['green'] =         bool(0b00001000 & b[5])
        data['right_yellow'] =     bool(0b00010000 & b[5])
        data['left_yellow'] =     bool(0b00100000 & b[5])
        
        data['num_matches'] = 0b11 & b[6]
        data['right_priorite'] = bool(0b0100 & b[6])
        data['left_priorite'] =  bool(0b1000 & b[6])
        
        data['right_red_penalty'] =         bool(0b0001 & b[8])
        data['left_red_penalty'] =      bool(0b0010 & b[8])
        data['right_yellow_penalty'] =  bool(0b0100 & b[8])
        data['left_yellow_penalty'] =   bool(0b1000 & b[8])
        
        # Check the checksum
        # Sum of the previous bytes without carry
        checksum_expected = 0xFF & sum(b[0:-1])
        
        if checksum_expected != b[9]:
            print "WARNING: checksum failure. Expected ", checksum_expected, ' but got ', b[9]
            return None
        
        #print ''
        #for (key, val) in data.iteritems():
            #print key, ': ', val
            
        return data
        
