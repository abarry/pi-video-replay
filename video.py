import sys
import io
import picamera
import time
import os
from subprocess import Popen
import pygame.display
import pygame.event
import pygame.font

import sys, traceback
import argparse
from read_favero_serial import ReadFaveroSerial
from threading import Lock


# Parse some command line flags
parser = argparse.ArgumentParser(description='Video replay system.')
parser.add_argument('-f', '--disable_fullscreen', action='store_true',
                    help='Disable fullscreen')
parser.add_argument('-v', '--disable_video', action='store_true',
                    help='Disable showing video')
parser.add_argument('-l', '--enable_lights', action='store_true',
                    help='Show text when a light is activated')

args = parser.parse_args()

script_path = os.path.dirname(os.path.realpath(__file__))
logo_path = os.path.join(script_path, 'logo.png')
if not args.disable_video:
    os.system('pngview -b 0x00000 -x 1680 -y 60 -l 3 ' + logo_path + ' &')
    #os.system('pngview -b 0x00000 -x 1480 -y 60 -l 3 ' + logo_path + ' &')


def update_screen(label):
    screen.fill((0, 0, 0))
    screen.blit(label, (200, 200))
    pygame.display.update()

running = True
replay = False
force_red = False

def handle_events(running, replay):
    for event in pygame.event.get():
        if (event.type == pygame.KEYUP and event.key == pygame.K_c and
            event.mod & pygame.KMOD_CTRL) or (event.type == pygame.KEYUP and event.key == pygame.K_ESCAPE) or (event.type == pygame.KEYUP and event.key == pygame.K_q):
            print 'keyboard q'
            running = False

        elif (event.type == pygame.KEYUP and (event.key == pygame.K_PAGEDOWN or event.key == pygame.K_PAGEUP or event.key == pygame.K_F5 or event.key == pygame.K_b)):
            replay = True
            force_red = True


    return (running, replay)

#set blank screen behind everything
pygame.init()
pygame.display.init()

# initialize font; must be called after 'pygame.init()' to avoid 'Font not Initialized' error
myfont = pygame.font.SysFont("monospace", 76)
smallfont = pygame.font.SysFont("monospace", 30)

if not args.disable_fullscreen:
    screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
else:
    screen = pygame.display.set_mode((1000, 400))
screen.fill((0, 0, 0))

#Set up confirguration
isoVal = 0
expmode = 'sports'
postseconds = 0.5
totalseconds = 2.5
thisframerate = 49
playbackframerate = 25
theoplaytime = totalseconds * thisframerate / playbackframerate
firstsleep = theoplaytime + 1
secsleep = firstsleep + 5

camera = picamera.PiCamera()
camera.vflip = True
camera.hflip = True
if args.enable_lights:
    #camera.annotate_size = 120
    #camera.annotate_foreground = picamera.Color('white')
    #camera.annotate_background = picamera.Color('black')
    #camera.annotate_text = "  GREEN    |     RED   "
    #camera.annotate_text = "                      |                      Red"
    pass
camera.resolution = (1296, 730)
camera.framerate = thisframerate
camera.exposure_mode = 'sports'
camera.iso = isoVal
stream = picamera.PiCameraCircularIO(camera, seconds=7)
camera.start_recording(stream, format='h264', intra_period = 10)
videocount = 1
#set up directory for files
rootdir = "/mnt/rd/"
dirpref = time.strftime("%b%d%y")
directory = rootdir + dirpref + "/"
dircreated = False
dirsuf = 0
#create directory
if not os.path.exists(directory):
    os.makedirs(directory)


mutex = Lock()

serial_triggered = False
last_green = False
last_red = False

both_str   = "       RED         |        GREEN      "
just_green = "                   |        GREEN      "
just_red   = "       RED         |                   "
none_str   = "                                       "

def set_light_text(red, green):
        if green and red:
            camera.annotate_background = picamera.Color('white')
            camera.annotate_text = both_str
            camera.annotate_foreground = picamera.Color('black')
        elif green:
            camera.annotate_background = picamera.Color('green')
            camera.annotate_text = just_green
            camera.annotate_foreground = picamera.Color('white')
        elif red:
            camera.annotate_text = just_red
            camera.annotate_background = picamera.Color('red')
            camera.annotate_foreground = picamera.Color('white')
        else:
            camera.annotate_text = none_str
            camera.annotate_background = None

def serial_callback(data):
    with mutex:
        global serial_triggered
        global last_red
        global last_green

        #last_frame = next(reversed(stream.frames))

        #text = ''
        if data['green'] == True and last_green == False:
            #text += 'green'=
            serial_triggered = True

        if data['red'] == True and last_red == False:
            #text += 'red'
            serial_triggered = True

        last_green = data['green']
        last_red = data['red']

    if args.enable_lights:
        set_light_text(data['red'], data['green'])

    #camera.annotate_text = text + ' ' + str(last_frame.timestamp)


# Setup serial port
serial = ReadFaveroSerial(serial_callback)
serial.setDaemon(True)
serial.start()


if not args.disable_video:
    camera.start_preview()
have_video = False
is_playing = False
label = myfont.render("Running...", 1, (255,255,0))


def killvideo():
    try:
        kill1 = Popen(['killall', 'omxplayer'])
    except:
        pass
    try:
        kill2 = Popen(['killall', 'omxplayer.bin'])
    except:
        pass

def kill_logo():
    try:
        kill1 = Popen(['killall', 'pngview'])
    except:
        pass

def cleanup():
    print 'Cleaning up.'
    label = myfont.render("Quit.", 1, (255,255,0))
    update_screen(label)
    serial.clean_up()
    killvideo()
    kill_logo()
    camera.stop_recording()
    camera.stop_preview()
    camera.close()
    pygame.display.quit()


update_screen(label)

try:
    while running:
        (running, replay) = handle_events(running, replay)

        camera.wait_recording(0.05)

        with mutex:
            triggered = replay | serial_triggered

        if triggered:
            print 'Preparing replay'
            label = myfont.render("Preparing replay #" + str(videocount) + " ...", 1, (255,255,0))
            update_screen(label)
            killvideo()

            #if args.enable_lights:
            #   set_light_text(True, True)

            is_playing = False
            replay = False
            serial_triggered = False
            have_video = True
            camera.wait_recording(postseconds)
            filemp4 = directory + "race" + str(videocount % 2) + ".mp4"
            filename = directory + "race" + str(videocount % 2) + ".h264"

            if os.path.exists(filemp4):
                os.remove(filemp4)
            if os.path.exists(filename):
                os.remove(filename)
            stream.copy_to(filename, seconds=totalseconds)
            convertstring = "MP4Box -fps " + str(playbackframerate) + " -add " + filename + " " + filemp4
            #playerstring = "omxplayer " + filemp4
            os.system(convertstring)
            #os.system(playerstring)
            videocount = videocount + 1
            camera.stop_preview()

        if have_video and not is_playing:
            print 'Playing video'

            if not args.disable_video:
                omxc = Popen(['omxplayer', '--no-osd', '--loop', filemp4])
            is_playing = True
            if args.enable_lights:
                set_light_text(False, False)

        update_screen(label)

except:
    print ''
    print 'Error!'
    ex_type, ex, tb = sys.exc_info()
    traceback.print_tb(tb)


cleanup()
